(TeX-add-style-hook "ref"
 (lambda ()
    (LaTeX-add-bibitems
     "PFUR-2011-2-kul"
     "PFUR-2011-2-kul::en"
     "denisov"
     "dubrovin"
     "logunov"
     "mors"
     "mors::en"
     "penrose-rindler-1987"
     "penrose-rindler-1987::en"
     "stratton:1948"
     "stratton:1948::en"
     "terletskiy-rybakov-1990"
     "vasiliev"
     "weinstein"
     "minkowski:1910"
     "minkowski:1910::en"
     "silberstein:1907"
     "silberstein:1907::en")))

